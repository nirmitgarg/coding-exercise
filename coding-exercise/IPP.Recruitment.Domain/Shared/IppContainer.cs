﻿using IPP.Recruitment.Domain.BusinessRules;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unity;


namespace IPP.Recruitment.Domain
{
    [ExcludeFromCodeCoverage]
    public static class IppContainer
    {
        public static IUnityContainer Configure()
        {            
            var container = new UnityContainer();
            
            container.RegisterType<IValidator, Validator>();

            return container;
        }

    }
}

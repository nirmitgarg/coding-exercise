﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IPP.Recruitment.Domain
{
    public static class Extension
    {
        public static bool IsBetween<T>(this T item, T start, T end)
        {
            return Comparer<T>.Default.Compare(item, start) >= 0
                && Comparer<T>.Default.Compare(item, end) <= 0;
        }

        public static bool IsGreaterThanEqual<T>(this T item, T minValue)
        {
            return Comparer<T>.Default.Compare(item, minValue) >= 0;
        }

        public static int CharToInt(this char item)
        {
            return item - '0';
        }

        public static bool IsEven(this int item)
        {
            return item % 2 == 0;
        }
        public static int DoubleAndSum(this int item)
        {
            return (item * 2).ToString().Select(x => x.CharToInt()).Sum();
        }

        public static bool IsValidMod10(this string item)
        {
            if (!string.IsNullOrEmpty(item))
            {
                var checkSum = item
                    .Select(ch => ch.CharToInt())
                    .Reverse()
                    .Select((digit, index) => index.IsEven() ? digit : digit.DoubleAndSum())
                    .Sum();

                return checkSum % 10 == 0;
            }
            return false;
        }
    }
}

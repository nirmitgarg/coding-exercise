﻿using IPP.Recruitment.Domain.BusinessRules;
using System;
using Unity;

namespace IPP.Recruitment.Domain
{
    /// <summary>
	/// The contract required to be implemented by a payment service
	/// </summary>
    public class PaymentService : IPaymentService
    {
        readonly IValidator _validator;

        public PaymentService(IValidator validator)
        {
            this._validator = validator;
        }

        public string WhatsYourId()
        {
            return Guid.NewGuid().ToString();
        }

        public Guid? MakePayment(CreditCard creditCard)
        {            
            _validator.AddRule(new IsCardNumberValid(creditCard.CardNumber));
            _validator.AddRule(new IsValidPaymentAmount(creditCard.Amount));
            _validator.AddRule(new CanMakePaymentWithCard(creditCard.CardNumber, creditCard.ExpiryMonth, creditCard.ExpiryYear));

            bool success = _validator.ExecuteRules();

            return success? Guid.NewGuid(): Guid.Empty;
        }
    }
}
﻿using IPP.Recruitment.Domain.BusinessRules;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IPP.Recruitment.Domain
{
    [ExcludeFromCodeCoverage]
    public class Validator : IValidator
    {
        List<IBusinessRule> _businessRules = new List<IBusinessRule>();

        public Validator()
        {

        }

        public void AddRule(IBusinessRule businessRule)
        {
            this._businessRules.Add(businessRule);
        }

        public bool ExecuteRules()
        {
            bool success = true;
            foreach (IBusinessRule rule in _businessRules)
            {
                success = rule.Validate();

                if (!success)
                    break;
            }
            return success;
        }
    }
}

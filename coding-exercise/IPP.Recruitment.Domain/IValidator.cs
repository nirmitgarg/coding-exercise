﻿using IPP.Recruitment.Domain.BusinessRules;

namespace IPP.Recruitment.Domain
{
    public interface IValidator
    {
        void AddRule(IBusinessRule businessRule);
        bool ExecuteRules();
    }
}
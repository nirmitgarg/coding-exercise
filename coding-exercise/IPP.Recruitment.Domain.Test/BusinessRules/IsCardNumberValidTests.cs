﻿
using IPP.Recruitment.Domain.BusinessRules;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Diagnostics.CodeAnalysis;
using Unity;

namespace IPP.Recruitment.Domain.Test
{
    [TestClass]
    [ExcludeFromCodeCoverage]
    public class IsCardNumberValidTests
    {       
        [TestMethod]
        public void Validate_CardNumberLessThan16Digit_ReturnsFalse()
        {
            //Arrange
            string cardNumber = "123456789";
            var rule = new IsCardNumberValid(cardNumber);

            //Act
            bool isValid = rule.Validate();

            //Assert
            Assert.IsFalse(isValid);
        }

        [TestMethod]
        public void Validate_CardNumberMoreThan16Digit_ReturnsFalse()
        {
            //Arrange
            string cardNumber = "123456789123456789";
            var rule = new IsCardNumberValid(cardNumber);

            //Act
            bool isValid = rule.Validate();

            //Assert
            Assert.IsFalse(isValid);
        }

        [TestMethod]
        public void Validate_CardNumberValidMod10_ReturnsTrue()
        {
            //Arrange
            string cardNumber = "5241330520823401";
            var rule = new IsCardNumberValid(cardNumber);

            //Act
            bool isValid = rule.Validate();

            //Assert
            Assert.IsTrue(isValid);
        }

        [TestMethod]
        public void Validate_CardNumberNotValidMod10_ReturnsFalse()
        {
            //Arrange
            string cardNumber = "5241330520823402";
            var rule = new IsCardNumberValid(cardNumber);

            //Act
            bool isValid = rule.Validate();

            //Assert
            Assert.IsFalse(isValid);
        }        
    }
}

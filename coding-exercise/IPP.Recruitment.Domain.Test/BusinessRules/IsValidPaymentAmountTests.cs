﻿
using IPP.Recruitment.Domain.BusinessRules;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Diagnostics.CodeAnalysis;
using Unity;


namespace IPP.Recruitment.Domain.Test
{
    [TestClass]
    [ExcludeFromCodeCoverage]
    public class IsValidPaymentAmountTests
    {       
        [TestMethod]
        public void Validate_AmountLessThan99_ReturnsFalse()
        {
            //Arrange
            long amount = 10;
            var rule = new IsValidPaymentAmount(amount);

            //Act
            bool isValid = rule.Validate();

            //Assert
            Assert.IsFalse(isValid);
        }
        [TestMethod]
        public void Validate_AmountMoreThan99999999_ReturnsFalse()
        {
            //Arrange
            long amount = 100000000;
            var rule = new IsValidPaymentAmount(amount);

            //Act
            bool isValid = rule.Validate();

            //Assert
            Assert.IsFalse(isValid);
        }
        [TestMethod]
        public void Validate_AmountBetween99And99999999_ReturnsTrue()
        {
            //Arrange
            long amount = 200;
            var rule = new IsValidPaymentAmount(amount);

            //Act
            bool isValid = rule.Validate();

            //Assert
            Assert.IsTrue(isValid);
        }        
    }
}

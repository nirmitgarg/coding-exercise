﻿
using IPP.Recruitment.Domain.BusinessRules;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Diagnostics.CodeAnalysis;
using Unity;


namespace IPP.Recruitment.Domain.Test
{
    [TestClass]
    [ExcludeFromCodeCoverage]
    public class CanMakePaymentWithCardTests
    {        
        [TestMethod]
        public void Validate_CardNumberNotValidMod10_ReturnsFalse()
        {
            //Arrange
            string cardNumber = "1234567891234567";
            int expiryMonth = 12;
            int expiryYear = 2019;
            var rule = new CanMakePaymentWithCard(cardNumber,expiryMonth,expiryYear);

            //Act
            bool isValid = rule.Validate();

            //Assert
            Assert.IsFalse(isValid);
        }
        [TestMethod]
        public void Validate_ExpiryMonthInvalid_ReturnsFalse()
        {
            //Arrange
            string cardNumber = "5241330520823401";
            int expiryMonth = 13;
            int expiryYear = 2019;
            var rule = new CanMakePaymentWithCard(cardNumber, expiryMonth, expiryYear);

            //Act
            bool isValid = rule.Validate();

            //Assert
            Assert.IsFalse(isValid);
        }
        [TestMethod]
        public void Validate_ExpiryYearMoreThanFourDigit_ReturnsFalse()
        {
            //Arrange
            string cardNumber = "5241330520823401";
            int expiryMonth = 12;
            int expiryYear = 20192;            
            var rule = new CanMakePaymentWithCard(cardNumber, expiryMonth, expiryYear);

            //Act
            bool isValid = rule.Validate();

            //Assert
            Assert.IsFalse(isValid);
        }
        [TestMethod]
        public void Validate_ExpiryYearPriorToCurrentYear_ReturnsFalse()
        {
            //Arrange
            string cardNumber = "5241330520823401";
            int expiryMonth = 12;
            int expiryYear = 2018;
            var rule = new CanMakePaymentWithCard(cardNumber, expiryMonth, expiryYear);

            //Act
            bool isValid = rule.Validate();

            //Assert
            Assert.IsFalse(isValid);
        }
        [TestMethod]
        public void Validate_ExpiryDatePriorToCurrentDate_ReturnsFalse()
        {
            //Arrange
            string cardNumber = "5241330520823401";
            int expiryMonth = 7;
            int expiryYear = 2019;
            var rule = new CanMakePaymentWithCard(cardNumber, expiryMonth, expiryYear);

            //Act
            bool isValid = rule.Validate();

            //Assert
            Assert.IsFalse(isValid);
        }
        [TestMethod]
        public void Validate_ValidCardNumberAndDate_ReturnsTrue()
        {
            //Arrange
            string cardNumber = "5241330520823401";
            int expiryMonth = 8;
            int expiryYear = 2019;
            var rule = new CanMakePaymentWithCard(cardNumber,expiryMonth, expiryYear);

            //Act
            bool isValid = rule.Validate();

            //Assert
            Assert.IsTrue(isValid);
        }

        [TestMethod]
        public void Validate_ValidCardNumberAndFutureDate_ReturnsTrue()
        {
            //Arrange
            string cardNumber = "5241330520823401";
            int expiryMonth = 1;
            int expiryYear = 2020;
            var rule = new CanMakePaymentWithCard(cardNumber, expiryMonth, expiryYear);

            //Act
            bool isValid = rule.Validate();

            //Assert
            Assert.IsTrue(isValid);
        }
    }
}

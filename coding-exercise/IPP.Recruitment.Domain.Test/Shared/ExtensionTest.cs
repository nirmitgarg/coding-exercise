﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace IPP.Recruitment.Domain.Test.Shared
{
    [TestClass]
    public class ExtensionTest
    {
        [TestMethod]
        public void IsBetween_InRange_ReturnsTrue()
        {
            //Arrange           
            int minValue = 100;
            int maxValue = 200;
            int numberToTest = 155;

            //Act
            bool result = numberToTest.IsBetween(minValue, maxValue);

            //Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void IsBetween_NotInRange_ReturnsFalse()
        {
            //Arrange           
            int minValue = 100;
            int maxValue = 200;
            int numberToTest = 55;

            //Act
            bool result = numberToTest.IsBetween(minValue, maxValue);

            //Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void IsGreaterThanEqual_InvalidNumber_ReturnsFalse()
        {
            //Arrange           
            int minValue = 100;
            int numberToTest = 55;

            //Act
            bool result = numberToTest.IsGreaterThanEqual(minValue);

            //Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void IsGreaterThanEqual_ValidNumber_ReturnsTrue()
        {
            //Arrange           
            int minValue = 54;
            int numberToTest = 55;

            //Act
            bool result = numberToTest.IsGreaterThanEqual(minValue);

            //Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void CharToInt_ValidNumber_ReturnsTrue()
        {
            //Arrange                       
            char valueToTest = '9';
            int expectedValue = 9;

            //Act
            int actual = valueToTest.CharToInt();

            //Assert
            Assert.AreEqual(expectedValue, actual);
        }

        [TestMethod]
        public void CharToInt_InvalidNumber_ReturnsTrue()
        {
            //Arrange                       
            char valueToTest = 'a';
            int expectedValue = 9;

            //Act
            int actual = valueToTest.CharToInt();

            //Assert
            Assert.AreNotEqual(expectedValue, actual);
        }

        public void IsEven_ValidNumber_ReturnsTrue()
        {
            //Arrange                       
            int valueToTest = 8;

            //Act
            bool result = valueToTest.IsEven();

            //Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void IsEven_InvalidNumber_ReturnsFalse()
        {
            //Arrange                       
            int valueToTest = 9;

            //Act
            bool result = valueToTest.IsEven();

            //Assert
            Assert.IsFalse(result);
        }
        [TestMethod]
        public void DoubleAndSum_DoubleMoreThanOneDigit_DigitsAdded()
        {
            //Arrange                       
            int valueToTest = 8;
            int expected = 7;

            //Act
            int actual = valueToTest.DoubleAndSum();

            //Assert
            Assert.AreEqual(expected, actual);
        }
        [TestMethod]
        public void DoubleAndSum_DoubleInSingleDigit_ValueDoubled()
        {
            //Arrange                       
            int valueToTest = 4;
            int expected = 8;

            //Act
            int actual = valueToTest.DoubleAndSum();

            //Assert
            Assert.AreEqual(expected, actual);
        }

        public void IsValidMod10_ValidNumber_ReturnsTrue()
        {
            //Arrange                       
            string valueToTest = "5241330520823401";

            //Act
            bool result = valueToTest.IsValidMod10();

            //Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void IsValidMod10_InvalidNumber_ReturnsFalse()
        {
            //Arrange                       
            string valueToTest = "5241330520823402";

            //Act
            bool result = valueToTest.IsValidMod10();

            //Assert
            Assert.IsFalse(result);
        }
    }
}

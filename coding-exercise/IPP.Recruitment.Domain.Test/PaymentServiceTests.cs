﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Unity;

namespace IPP.Recruitment.Domain.Test
{
    [TestClass]
    public class PaymentServiceTests
    {
        private Mock<IValidator> mockValidator;

        [TestInitialize]
        public void Startup()
        {
            mockValidator = new Mock<IValidator>();
        }

        [TestMethod]
        public void MakePayment_ValidCardAndAmount_ReturnsNewGuid()
        {
            //Arrange            
            var paymentService = new PaymentService(mockValidator.Object);
            var creditCard = new CreditCard
            {
                Amount = 200,
                CardNumber = "5241330520823401",
                ExpiryMonth = 12,
                ExpiryYear = 2019
            };
            
            mockValidator.Setup(m => m.ExecuteRules()).Returns(true);

            //Act
            var result = paymentService.MakePayment(creditCard);

            //Assert
            mockValidator.Verify(mock => mock.ExecuteRules(), Times.Once());
            Guid guidResult;
            Assert.IsTrue(Guid.TryParse(result.ToString(), out guidResult));
        }


        [TestMethod]
        public void MakePayment_ValidCardAndAmount_ReturnsEmptyGuid()
        {
            //Arrange            
            var paymentService = new PaymentService(mockValidator.Object);

            var creditCard = new CreditCard
            {
                Amount = 200,
                CardNumber = "5241330520823401",
                ExpiryMonth = 12,
                ExpiryYear = 2019
            };

            
            mockValidator.Setup(m => m.ExecuteRules()).Returns(false);

            //Act
            var result = paymentService.MakePayment(creditCard);

            //Assert            
            mockValidator.Verify(mock => mock.ExecuteRules(), Times.Once());
            Assert.AreEqual(result, Guid.Empty);
        }

        [TestMethod]
        public void WhatIsYourId_ReturnsNonEmptyGuid()
        {
            //Arrange            
            var paymentService = new PaymentService(mockValidator.Object);

            //Act
            var result = paymentService.WhatsYourId();

            //Assert                        
            Assert.AreNotEqual(result, Guid.Empty);
        }
    }
}
